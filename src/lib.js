
import prettyjson from 'prettyjson'
import { get, last } from 'lodash/fp'

/**
 * Print an error from a GraphQL client
 * @author Angelo Ashmore
 */

export const printGraphQLError = e => {
    const prettyjsonOptions = { keysColor: 'red', dashColor: 'red' }
    
        if (e.response && e.response.errors)
        console.error(prettyjson.render(e.response.errors, prettyjsonOptions))
    
        if (e.request) console.error(prettyjson.render(e.request, prettyjsonOptions))
    }

/**
 * Request a query from a client.
 * @author Angelo Ashmore
 */

function sleep (time) {
    return new Promise((resolve) => setTimeout(resolve, time));
}

export const queryOnce = async ({ client, query, args = {first: 250}, attempts = 2 }) => {
    console.log("Query Once, attempt " + attempts)
    return new Promise((resolve, reject) => {
        client.rawRequest(query, args)
            .then(response => {
                resolve(response)
            })
            .catch( async (error) => {
                let { errors, extensions } = error.response

                if (errors && extensions) {
                    
                    let queryCost = extensions.cost.requestedQueryCost
                    let { currentlyAvailable: available, restoreRate } = extensions.cost.throttleStatus

                    await sleep(Math.max((queryCost - available)/restoreRate) * 1000, 5)

                    if (attempts === 1) return reject(error) // base case
                    queryOnce({ client, query, args, attempts: attempts - 1 })
                        .then(response => resolve(response))
                        .catch(reject)

                } else {
                    reject(error)
                }
            })
    })
}

/**
 * Get all paginated data from a query. Will execute multiple requests as
 * needed.
 * @author Angelo Ashmore
 */

export const queryAll = async(
    client,
    path,
    query,
    args = {first: 250},
    aggregatedResponse,
) => {
    const { data, errors, extensions, headers, status } = await queryOnce({ client, query, args })

    const edges = get([...path, 'edges'], data)
    const nodes = edges.map(edge => edge.node)

    aggregatedResponse
        ? (aggregatedResponse = aggregatedResponse.concat(nodes))
        : (aggregatedResponse = nodes)

    if (get([...path, 'pageInfo', 'hasNextPage'], false, data)){
        args.after = last(edges).cursor;
        return queryAll(
            client, 
            path, 
            query, 
            args, 
            aggregatedResponse
        )
    }

    return aggregatedResponse
}
