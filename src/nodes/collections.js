
// external libs

import { forEach, map } from 'p-iteration'
import { get } from 'lodash/fp'

// internal libs

import { queryAll } from '../lib'
import { downloadImageAndCreateFileNode } from './file'

export const createCollectionNodes = async ({ clients, nodeHelpers, imageHelpers, debugHelpers }) => {

    const CollectionNode = nodeHelpers.createNodeFactory("COLLECTION", async node => {
        if (node.image)
            node.image.localFile___NODE = await downloadImageAndCreateFileNode({ 
                id: node.image.id, 
                url: node.image.src,
                prefix: nodeHelpers.TYPE_PREFIX,
                ...imageHelpers
            })
    
        return node;
    });

    await forEach(
        await queryAll(clients.storefront, ['shop', 'collections'], queryCollections),
        async collection => {

            if (collection.products)
                collection.products___NODE = collection.products.edges.map(edge =>
                    nodeHelpers.generateNodeId("PRODUCT", edge.node.id),
                )

            const node = await CollectionNode(collection)
            nodeHelpers.createNode(node)
        },
    )
}

const queryCollections = `
query($first: Int!, $after: String) {
    shop {
        collections(first: $first, after: $after) {
            pageInfo {
                hasNextPage
            }
            edges {
                cursor
                node {
                    id
                    handle
                    title
                    description
                    descriptionHtml
                    image {
                        id
                        altText
                        src
                    }
                    products(first: 50) {
                        edges {
                            node {
                                id
                            }
                        }
                    }
                }
            }
        }
    }
}
`;

