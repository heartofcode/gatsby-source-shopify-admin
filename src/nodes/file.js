
// external libs

import { createRemoteFileNode } from 'gatsby-source-filesystem'

export const downloadImageAndCreateFileNode = async ({ prefix = "", id, url, createNode, touchNode, store, cache }) => {
    
    let fileNodeID

    const mediaDataCacheKey = `${prefix}__Media__${url}`
    const cacheMediaData = await cache.get(mediaDataCacheKey)

    if (cacheMediaData) {
        fileNodeID = cacheMediaData.fileNodeID
        touchNode(fileNodeID)

        return fileNodeID
    }

    const fileNode = await createRemoteFileNode({ url, store, cache, createNode })

    if (fileNode) {
        fileNodeID = fileNode.id
        await cache.set(mediaDataCacheKey, { fileNodeID })

        return fileNodeID
    }

    return undefined
}