export const createPolicyNodes = () => {
    
}

const policies = `
{
    shop {
        privacyPolicy {
            body
            id
            title
            url
        }
        refundPolicy {
            body
            id
            title
            url
        }
        termsOfService {
            body
            id
            title
            url
        }
    }
}
`;